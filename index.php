<?php
$strng = "Hello world!";
$int = 1984;
$flt = 99.999;
$boo_t = true;
$boo_f = false;
$numbers = array(51, 5, 4, 444, 522, 16, 7, 28, 11);
$print_color = array("#f44242", "#e8f441", "#41f449", "#41a3f4");

include 'function.php';
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="PHP domaci - funkcije i varijabli.">
        <meta name="keywords" content="php, functions, variables">
        <meta name="author" content="Atila Alacan">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PHP domaci - funkcije i varijabli</title>
        <link href="https://fonts.googleapis.com/css?family=Fira+Mono|Fira+Sans" rel="stylesheet">
        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/styles.css">
    </head>
    <body>
        <nav></nav>
        <main>
            <h2>PHP variables</h2>

            <p><strong>String</strong></p>
            <div class="code code--strng">
                <?php echo "\"$strng\" is a string."; ?>
            </div>

            <p><strong>Integer</strong></p>
            <div class="code code--int">
                <?php echo "\"$int\" is an integer."; ?>
            </div>

            <p><strong>Float</strong></p>
            <div class="code code--flt">
                <?php echo "\"$flt\" is a float."; ?>
            </div>

            <p><strong>Boolean</strong></p>
            <div class="code code--boo">
                <?php
                echo "\"" . json_encode($boo_t) . "\" is a boolean.<br>";
                echo "\"" . json_encode($boo_f) . "\" is also a boolean.";
                ?>
            </div>

            <p><strong>Array (with echo)</strong></p>
            <div class="code code--numb">
                <?php echo "\"" . $numbers[0] . ", " . $numbers[1] . ", " . $numbers[2] . ", " . $numbers[3] . ", " . $numbers[4] . " and " . $numbers[5] . "\" is an array."; ?>
            </div>

            <p><strong>Array (with foreach)</strong></p>
            <div class="code code--numb">
                <?php
                        foreach ($numbers as $value) {
                            echo "$value <br>";
                        }
                ?>
            </div>

            <h2>PHP functions</h2>
            <p><strong>Function sumColor</strong> with two variables: first one is a string ($color), the second one is the array from above ($numbers).</p>
            <div class="code code--numb">
            <?php echo sumColor($print_color[0], $numbers); ?>
            </div>

            <p><strong>Function avrgColor</strong></p>
            <div class="code code--numb">
            <?php echo avrgColor($print_color[1], $numbers); ?>
            </div>

            <p><strong>Function maxColor</strong></p>
            <div class="code code--numb">
            <?php echo maxColor($print_color[2], $numbers); ?>
            </div>

            <p><strong>Function minColor</strong></p>
            <div class="code code--numb">
            <?php echo minColor($print_color[3], $numbers); ?>
            </div>
        </main>
    </body>
</html>