<?php

function sumColor($color, $numbers) {  
    $sum = 0;
    for($x = 0; $x < count($numbers); $x++) {
        $sum += $numbers[$x];
        $result = "<span style=\"color:$color\">$sum</span>";
    }
    return $result;
}

function avrgColor($color, $numbers) {  
    $sum = 0;
    for($x = 0; $x < count($numbers); $x++) {
        $sum += $numbers[$x];
        $avg = $sum / count($numbers);
        $result = "<span style=\"color:$color\">$avg</span>";
    }
    return $result;
}

function maxColor($color, $numbers) {  
    $max = NULL;
    for($x = 0; $x < count($numbers); $x++) {
        if ($numbers[$x] > $max) {
            $max =  $numbers[$x];
            $result = "<span style=\"color:$color\">$max</span>";
        }
    }
    return $result;
}

function minColor($color, $numbers) {  
    $min = $numbers[0];
    for($x = 0; $x < count($numbers); $x++) {
        if ($numbers[$x] < $min) {
            $min =  $numbers[$x];
            $result = "<span style=\"color:$color\">$min</span>";
        }
    }
    return $result;
}

?>