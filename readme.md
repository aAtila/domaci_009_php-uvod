#Domaći zadatak

* Napraviti web stranicu koja sadrži definicije promeljnivih tipa: int, string, float, bool i array.
* Vrednosti promenljivih ispisati različitim bojama.
* Definisati funkciju koja prima dva parametra, prvi je tipa string i sadrži engleski naziv boje (red, blue ili green) a drugi parametar je niz sa brojevima.
* Funkcija treba da izračuna sumu tih brojeva i da je ispiše u boji koja je prosleđena preko prvog parametra.
* Definiciju funkcije staviti u poseban fajl (functions.php) koji je potrebno uključiti u ovaj glavni fajl.